# lemoulin-extras #

- cache
- dynamic_settings
- gallery
- l10n
- pages
- shutdown
- utils

## Requirements ##

- Python >= 3.4
- Django >= 1.9, < 1.10
- requests
- django-meta (for pages only)

## Installation ##

```
$ pip install git+https://bitbucket.org/lemoulin/django-lemoulin-extras
```

in `settings.py`:

```
INSTALLED_APPS = (
    # ...
    'lemoulin_extras',
)
```

## Usage ##

View `README.md` for each extra.

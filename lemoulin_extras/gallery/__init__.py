import requests
import re

default_app_config = 'lemoulin_extras.gallery.apps.GalleryConfig'


def get_image_details(image_url):
	image_url = image_url.strip()

	if image_url.lower().count("instagram") > 0:
		data = get_instagram_details(image_url=image_url)
	else:
		data = {
			"source": "url",
		}

	return data


def get_video_details(video_url):
	video_url = video_url.strip()

	if video_url.lower().count("vimeo") > 0:
		data = get_vimeo_details(video_url=video_url)
	elif video_url.lower().count("youtube") > 0:
		data = get_youtube_details(video_url=video_url)
	else:
		data = {}

	return data


def get_youtube_details(video_url):
	from urllib.parse import urlparse, parse_qs
	oembed_data = call_youtube_oembed_api(video_url=video_url)

	# get video id
	parsed_url = urlparse(video_url)
	try:
		video_id = parse_qs(parsed_url.query)['v'][0]
	except:
		video_id = None

	response = {}
	response["source"] = "youtube"
	response["title"] = oembed_data.get("title", None)
	response["description"] = oembed_data.get("description", None)
	response["width"] = oembed_data.get("width", 0)
	response["height"] = oembed_data.get("height", 0)
	response["thumbnail"] = oembed_data.get("thumbnail_url")
	response["duration"] = 0
	response["plays"] = 0
	response["likes"] = 0
	response["id"] = None
	response["video_id"] = video_id

	return response


def call_youtube_oembed_api(video_url):
	api_url = "http://www.youtube.com/oembed"

	params = {"url": video_url, "format": "json"}

	try:
		return requests.get(api_url, params=params).json()
	except:
		return {}


def get_vimeo_details(video_url):
	response = {}
	video_id = get_vimeo_video_id_from_url(video_url=video_url)

	if video_id:
		video_data = call_vimeo_simple_api(video_id=video_id)

		if video_data:
			response["source"] = "vimeo"
			response["title"] = video_data.get("title", None)
			response["description"] = video_data.get("description", None)
			response["width"] = video_data.get("width", 0)
			response["height"] = video_data.get("height", 0)
			response["thumbnail"] = video_data.get("thumbnail_large")
			response["duration"] = video_data.get("duration", 0)
			response["plays"] = video_data.get("stats_number_of_plays", 0)
			response["likes"] = video_data.get("stats_number_of_likes", 0)
			response["video_id"] = video_data.get("id", None)
	else:
		return "error"

	return response


def get_vimeo_video_id_from_url(video_url):
	regex = re.compile("([\d]+)")

	try:
		video_id = regex.findall(video_url)[0]
	except:
		video_id = None

	return video_id


def call_vimeo_oembed_api(video_url):
	api_url = "http://vimeo.com/api/oembed.json?url=%s" % (video_url)

	try:
		return requests.get(api_url).json()
	except:
		return {}


def call_vimeo_simple_api(video_id):
	api_url = "http://vimeo.com/api/v2/video/%s.json" % (video_id)

	try:
		return requests.get(api_url).json()
	except:
		return {}


def get_instagram_details(image_url):
	response = {}

	image_data = call_instagram_oembed_api(image_url=image_url)

	response["source"] = "instagram"
	response["title"] = image_data.get("title", None)
	response["thumbnail_url"] = image_data.get("thumbnail_url", None)
	response["embed_html"] = image_data.get("html", None)
	response["author_url"] = image_data.get("author_url", None)

	return response


def call_instagram_oembed_api(image_url):
	api_url = "http://api.instagram.com/publicapi/oembed/?url=%s" % (image_url)

	try:
		return requests.get(api_url).json()
	except:
		return {}

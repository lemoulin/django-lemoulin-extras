from django.apps import AppConfig


class L10NConfig(AppConfig):
    name = 'lemoulin_extras.l10n'
